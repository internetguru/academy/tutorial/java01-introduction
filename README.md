[![pipeline status](https://gitlab.com/internetguru/academy/tutorial/java01-introduction/badges/master/pipeline.svg)](https://gitlab.com/internetguru/academy/tutorial/java01-introduction/-/pipelines?ref=master)
[![compile](https://gitlab.com/internetguru/academy/tutorial/java01-introduction/builds/artifacts/master/raw/.results/compile.svg?job=evaluate)](https://gitlab.com/internetguru/academy/tutorial/java01-introduction/-/jobs/artifacts/master/file/.results/compile.log?job=evaluate)
[![checkstyle](https://gitlab.com/internetguru/academy/tutorial/java01-introduction/builds/artifacts/master/raw/.results/checkstyle.svg?job=evaluate)](https://gitlab.com/internetguru/academy/tutorial/java01-introduction/-/jobs/artifacts/master/file/.results/checkstyle.log?job=evaluate)
[![test](https://gitlab.com/internetguru/academy/tutorial/java01-introduction/builds/artifacts/master/raw/.results/test.svg?job=evaluate)](https://gitlab.com/internetguru/academy/tutorial/java01-introduction/-/jobs/artifacts/master/file/.results/test.log?job=evaluate)

# Lab01: Introduction

> This project is designed to introduce programing environment to students. It prints out `Hello world!` string and simulates a coin flip with an uneven probability. Additionally, it analyzes arrays of integers searching for specific value pairs.
